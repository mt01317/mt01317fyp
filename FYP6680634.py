#Stress and its impact on phishing susceptibility: Megan Tyrrell. 6680634. mt01317@surrey.ac.uk

##### Read Me #####
#please use "pip install pandas" to run this program


from math import sqrt #For finding standard deviation
import csv #For reading datasets            csv.reader(file, dialect='excel', **parameters) 
import pandas as pd
import matplotlib.pyplot as plt #Imports for streamlining mathematical calculations
#import numpy as py
import math


    

def standardDeviationFunction(sdDf): #Linked to timeFunction, calculates and builds a standard deviation chart
                        #Possible Ref -> statistics.stdev()
    #sdDf=sdDf.groupby('TimePressure')['No Correct'] #A sample standard deviation. Due to the sample size.
    mean = sdDf.mean() #Finds Mean value 
    sdDf.std() #Simple standardDeviation Function

    print(sdDf.std())
    
    
def boxPlotFunction(tdf,gtdf,labelA,labelB): #Linked to timeFunction, calculates and builds a box plot #Sends over time dataframe and grouped dataframe
    print('Creating Box Plot')
    ax=tdf.boxplot(column='No Correct',by = 'TimePressure',) #Creates a boxplot, sets values
    ax.set_xticks([1,2]);ax.set_xticklabels([labelA,labelB]) #Changes labels from 1 and 2
    ax.set_ylabel('Amount Correct')  #Y axis label
    plt.title('Time Pressure Influence on Susceptibility')
    plt.suptitle('') #Removes uneeded Titles
    
    print('--------------------------------------------------------') #Prints Mean to terminal. Not sure if neccesary if doing bar chart.
    print('Dataset A Mean: Correct choices under pressure. 1-Time Pressure. 2-No Time Pressure')
    print(gtdf)
    print('--------------------------------------------------------')
   
    input('Press any key to continue') #Easy reading of Mean values
    

def barChartFunction(gdf,labelA,labelB): #Linked to timeFunction, calculates and builds a bar chart -Labels so different datasets may use different names
    print('Creating Bar Chart')
    a=gdf.plot(kind='barh',) #Simple. takes dataset passed in and plots a bar chart
   # plt.tick_params(axis='x', which='major', bottom=False,labelbottom=False) Additional labeling techniques

    a.set_yticks([0,1]);a.set_yticklabels([labelA,labelB]) #Passed in labels specific to each dataset

def totalFunction(): #Final Section. Print Charts and summarise
    print('Dataset analytics complete')
    input('Press any key to see the graphs from Dataset B')
    plt.show() #Simple. Displays Dataset B

def timeFunction(): # The Dataset configuration for Time pressure calculations. Dataset A
    print('Analysing Time Pressure Dataset A')
    labelA = 'Time\nPressure' #For function reusability
    labelB = 'No\nTime\npressure'
    tdf = pd.read_csv('EmailTaskData_PLOSOne.csv', index_col=0) #Pandas. Reads csv file.
    print('Dataset A: Time Pressure')
    print(tdf) #Prints initial version
    gtdf=tdf.groupby('TimePressure')['No Correct'].mean() #grouped time dataframe and mean
   # barChartFunction(gtdf,labelA,labelB) #Simple bar chart displaying difference of averages
    a=gtdf.plot(kind='barh')
    a.set_yticks([0,1]);a.set_yticklabels(['Time\nPressure','No\nTime\npressure'])
    plt.suptitle('Dataset A: Time Pressure') #bar chart label
    plt.xlabel('Amount Correct')
    boxPlotFunction(tdf,gtdf,labelA,labelB) #Box Plot. Send dataframe and Average dataframe
    print('The standard deviation of Dataset A:')
    sdDf=tdf.groupby('TimePressure')['No Correct']
    standardDeviationFunction(sdDf)

    input('Press any key to see the graphs from Dataset A')
    plt.show()


def urgencyFunction(): # The Dataset configuration for urgency calculations
    print('Analysing Urgency Dataset B')
    uDfA = pd.read_csv('dataverse scam data 1.2.csv') #For urgency/authority index removed?
    print("Here is the base Dataset B before edits to make it more streamlined.\nThese edits include generalising categories and user choice")
    print(uDfA) #Test
    input('Press any key to continue')

    to_rep=dict(zip([1,2,3,4],['Safe','Safe','Scam','Scam'])) #Makes user choice clearer
    uDfA.replace({'userId':to_rep}, inplace=True)
   # print(uDfA) #Test


    uDfA=uDfA.drop(uDfA[uDfA['category']=='Phishing Emails'].index) #Remove "Phishing email " Category -> Too vague
    #Adding a column to generalise Urgency. Tweak later
    uDfA.insert(7,'Urgency/Authority','X') #X to be replaced
    uDfA.loc[uDfA['category']=='General', 'Urgency/Authority']='No Pressure'
    uDfA.loc[uDfA['category']=='Social', 'Urgency/Authority']='No Pressure'
    uDfA.loc[uDfA['category']=='Ideological', 'Urgency/Authority']='No Pressure'
    uDfA.loc[uDfA['category']=='Lottery & Sweepstakes', 'Urgency/Authority']='No Pressure'
    uDfA.loc[uDfA['category']=='Lottery and Sweepstakes', 'Urgency/Authority']='No Pressure'
    uDfA.loc[uDfA['category']=='Financial', 'Urgency/Authority']='Pressure'
    uDfA.loc[uDfA['category']=='Finance and Banking', 'Urgency/Authority']='Pressure'   
    uDfA.loc[uDfA['category']=='Security', 'Urgency/Authority']='Pressure'
    uDfA.loc[uDfA['category']=='University Emails', 'Urgency/Authority']='Pressure'
    uDfA.loc[uDfA['category']=='University Phishing Emails', 'Urgency/Authority']='Pressure'
    uDfA.loc[uDfA['category']=='Health', 'Urgency/Authority']='Pressure'
    uDfA.loc[uDfA['category']=='Job Offer', 'Urgency/Authority']='Pressure'
    uDfA.loc[uDfA['category']=='Legal', 'Urgency/Authority']='Pressure'
 

    #Compare user choice
    uDfA.insert(8,'Correct','')
    print('Here is the edited Dataset B')
    uDfA['Correct']=uDfA.apply(correctFunction, axis=1) #Create a new column. link to correctFunction.
    print(uDfA)
    input('Press any key to continue')

    #Urgency and react time. Does stress mean people spend less time?

    labelA='No\nUrgency'
    labelB='Urgency'
    guDfA=uDfA.groupby('Urgency/Authority')['reactTime'].mean()
    barChartFunction(guDfA,labelA,labelB)
    plt.suptitle('Dataset B: Urgency') #bar chart label
    plt.xlabel('Time Spent')
   # print(guDfA)
    #Box plot creation. Not using function due to possible overlaps
    print('Creating Box Plot')
    ab=uDfA.boxplot(column='reactTime',by = 'Urgency/Authority',) #Creates a boxplot, sets values
    ab.set_xticks([1,2]);ab.set_xticklabels([labelA,labelB]) #Changes labels from 1 and 2
    ab.set_ylabel('React Time')  #Y axis label
    plt.title('Urgency Influence on Susceptibility')

    #What is the standard deviation of react times?
    sdDf=uDfA.groupby('Urgency/Authority')['reactTime']
    print('The standard deviation of Dataset B:')
    standardDeviationFunction(sdDf)
    input('Press any key to continue') #Pause

    
    print('--------------------------------------------------------') #Prints Mean to terminal. 
    print('Dataset B Mean: React Time under pressure.')
    print(guDfA)
    print('--------------------------------------------------------')
   
    input('Press any key to continue') #Easy reading of Mean values




    urgentscamCorrectCount = 0 #For comparing urgent and non urgent. A counter
    urgentscamWrongCount= 0
    urgentSafeCorrectCount = 0
    urgentSafeWrongCount = 0
    scamCorrectCount = 0
    scamWrongCount= 0
    SafeCorrectCount = 0
    SafeWrongCount = 0


    correctList=uDfA['Correct'].tolist()
    emailList = uDfA['realId'].tolist() #To urgent
    urgentList = uDfA['Urgency/Authority'].tolist()
    for a in range(len(urgentList)):
        if urgentList[a] == "Pressure":
            if correctList[a] == 1: #
            
                if emailList[a] =='Scam':
                    urgentscamCorrectCount +=1
                if emailList[a] == 'Safe':
                    urgentSafeCorrectCount +=1
            if correctList[a] == 0:
                
                if emailList[a] =='Safe':
                    urgentSafeWrongCount +=1
                if emailList[a] == 'Scam':
                    urgentscamWrongCount +=1
        if urgentList[a] == "No Pressure":            
            if correctList[a] == 0:
                
                if emailList[a] =='Safe':
                    SafeWrongCount +=1
                if emailList[a] == 'Scam':
                    scamWrongCount +=1
            if correctList[a] == 1: 
            
                if emailList[a] =='Scam':
                    scamCorrectCount +=1
                if emailList[a] == 'Safe':
                    SafeCorrectCount +=1

    scamTotal = scamWrongCount + scamCorrectCount # Turn to urgent total
    safeTotal = SafeWrongCount +SafeCorrectCount #Turn to non urgent total
    urgentscamTotal = urgentscamWrongCount + urgentscamCorrectCount # Turn to urgent total
    urgentsafeTotal = urgentSafeWrongCount +urgentSafeCorrectCount #Turn to non urgent total
    urgentTotal = urgentscamTotal +urgentsafeTotal
    nonurgentTotal=scamTotal +safeTotal


    scamCorrectPercentage = (scamCorrectCount/nonurgentTotal)*100
    scamWrongPercentage = (scamWrongCount/nonurgentTotal)*100
    safeCorrectPercentage = (SafeCorrectCount/nonurgentTotal)*100
    safeWrongPercentage= (SafeWrongCount/nonurgentTotal) *100
    urgentscamCorrectPercentage = (urgentscamCorrectCount/urgentTotal)*100
    urgentscamWrongPercentage = (urgentscamWrongCount/urgentTotal)*100
    urgentsafeCorrectPercentage = (urgentSafeCorrectCount/urgentTotal)*100
    urgentsafeWrongPercentage= (urgentSafeWrongCount/urgentTotal) *100


    abba = pd.DataFrame([('Scam Correct',urgentscamCorrectPercentage,scamCorrectPercentage),('Scam Incorrect',urgentscamWrongPercentage,scamWrongPercentage ),('Safe Correct',urgentsafeCorrectPercentage,safeCorrectPercentage),('Safe Incorrect',urgentsafeWrongPercentage,safeWrongPercentage)], columns = ('Answer', 'Pressure','No Pressure'))
    abba.plot.bar(rot=0,x='Answer')
    plt.suptitle('Dataset B: Urgency')
    print('Dataset B: The Average amount of Correct and Incorrect Choices between urgent and non urgent.')
    print(abba)
    plt.xlabel('Urgency and Susceptibility')
    plt.ylabel('Decision %')


def correctFunction(row):  #Works out if participant choice was correct
    if row['userId'] == row['realId']:
        return 1
    return 0


def stagingFunction(): #A list of all datasets, and what values to pass around. Order of functions
    timeFunction() #Datasets from Time Pressure
    urgencyFunction() #Datasets for Urgency
    totalFunction() #Display


def startFunction(): #Where the program begins. Contains an explanation, any setting up and sending onto the next segment
    print("Stress and its impact on phishing susceptibility: Megan Tyrrell mt01317@surrey.ac.uk, 6680634\n")
    print('\nThis program exists to analyse the impact of stress on phishing susceptibility.')
    print('\nIn this program, stress is measured through time pressure, urgency and email load ')
    print("This program includes Dataset A: EmailTaskData_PLOSOne.csv and Dataset B: dataverse scam data 1.2.csv.")
    print("Dataset A(Jones et al., 2018)\nDataset B(Hakim et al., 2020)\n")

    print("To highlight possible changes to susceptibility under stress,") 
    print("this program makes use of box plots, bar charts and standard deviation.")
    print("### Segments ###\nIntroduction\nDataset A\nDataset B\nEnd\n###")
    
    input("Press any key to start") #Ready Button. To allow for moments of pause
    stagingFunction() #Send to->
    




startFunction() #Used to begin the program.